var baseUrl     = window.location.origin;
var uriSegments = window.location.pathname.split('/');
baseUrl        += '/' + uriSegments[1] + '/';

$(document).ready(function() {
	
	// Handle click on delete action in table sch-table
	$(document).on('click', '#sch-table a#delete', function(ev) {
		ev.preventDefault();
		
		var idSched = $(this).data('id');
		var jam     = $(this).closest('tr').find('td[data-name=jam]').text();

		$.confirm({
			title: 'Perhatian',
			content: 'Hapus jadwal makan pada jam: ' + jam + ' ?',
			buttons: {
				batal: function() {

				},
				hapus: {
					btnClass: 'btn-red',
					action: function() {

						$.LoadingOverlay('show');
						$.ajax({
							url: baseUrl + 'setting_jadwal/delete',
							type: 'POST',
							data: {id: idSched},
						})
						.done(function() {
							$.LoadingOverlay('hide');
							location.reload();
						})
						.fail(function() {
							$.LoadingOverlay('hide');
							console.log("error");
						});
						
					}
				}
			}
		});
	});

});