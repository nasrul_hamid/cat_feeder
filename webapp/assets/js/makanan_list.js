var baseUrl     = window.location.origin;
var uriSegments = window.location.pathname.split('/');
baseUrl        += '/' + uriSegments[1] + '/';

$(document).ready(function() {
    
    $(document).on('click', '#food-table a#delete', function(e){
        e.preventDefault();

        var nama      = $(this).closest('tr').find('td[data-name=nama]').text();
        var idMakanan = $(this).data('id');
        
        $.confirm({
            title: 'Perhatian',
            content: 'Anda yakin hapus makanan dengan nama ' + nama + ' ?',
            buttons: {
                batal: function() {

                },
                hapus: {
                    btnClass: 'btn-red',
                    action: function() {

                        $.LoadingOverlay('show');
						$.ajax({
							url: baseUrl + 'makanan/delete',
							type: 'POST',
							data: {id: idMakanan},
						})
						.done(function() {
							$.LoadingOverlay('hide');
							location.reload();
						})
						.fail(function() {
							$.LoadingOverlay('hide');
							console.log("error");
						});
                    }
                }
            }
        });
    })
});