var baseUrl     = window.location.origin
var uriSegments = window.location.pathname.split('/');
baseUrl        += '/' + uriSegments[1] + '/';

$(document).ready(function() {

	$(document).on('click', '#alat-table a#delete', function(event) {
		event.preventDefault();

		var idAlat = $(this).data('id');
		var nama   = $(this).closest('tr').find('td[data-name=nama]').text();

		$.confirm({
			title: 'Perhatian',
			content: 'Anda yakin akan hapus alat ' + nama + ' ?',
			buttons: {
				batal: function() {

				},
				hapus: {
					btnClass: 'btn-red',
					action: function() {
						$.LoadingOverlay('show');
						$.ajax({
							url: baseUrl + 'alat/delete',
							type: 'POST',
							data: {id: idAlat},
						})
						.done(function() {
							$.LoadingOverlay('hide');
							location.reload();
						})
						.fail(function() {
							$.LoadingOverlay('hide');
							console.log("error");
						});
					}
				}
			}
		});


		
		
	});
});