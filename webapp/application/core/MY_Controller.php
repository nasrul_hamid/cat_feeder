<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/**
     * Print JSON as response for front end request
     * 
     * @param  array  $data      array of data to print as JSON
     * @param  [type] $http_code HTTP code for indication response type
     * 
     */
	protected function response($http_code = NULL, $data = NULL)
	{
		$this->output
		      ->set_status_header($http_code)
		      ->set_content_type('application/json', 'utf-8')
		      ->set_output(json_encode($data, JSON_PRETTY_PRINT))
		      ->_display();
		    exit;
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */