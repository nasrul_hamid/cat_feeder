<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('ch_is_empty'))
{
   function ch_is_empty($data)
   {
       return ( isset($data) ) ? $data : NULL;
   }
}

if ( ! function_exists('ch_log'))
{
   function ch_log($data)
   {
       log_message('error', print_r($data, TRUE));
   }
}

if ( ! function_exists('ch_set_flash'))
{
	function ch_set_flash($flag = "", $msg)
	{
		$CI =& get_instance();
		$CI->load->library('session');

		if($flag != "")
		{
			if($flag == 'success')
			{
				$CI->session->set_flashdata(
					'alert', 
					array(
						'msg'=>$msg,
						'type'=>'alert-success',
						'icon'=>'fa-check',
						'title'=>'Success'
						)
					);
			} else
			{
				$CI->session->set_flashdata(
					'alert', 
					array(
						'msg'=>$msg,
						'type'=>'alert-danger',
						'icon'=>'fa-ban',
						'title'=>'Failed'
						)
					);
			}
		}
	}
}

if ( ! function_exists('ch_falert'))
{
	function ch_falert()
	{
		$CI =& get_instance();
		$CI->load->library('session');
		$alert = $CI->session->flashdata('alert');
		if ($alert != null) {
			echo '
			<div class="alert '.$alert['type'].' alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa '.$alert['icon'].'"></i> '.$alert['title'].'</h4>
			'.$alert['msg'].'
			</div>';
		}
	}
}