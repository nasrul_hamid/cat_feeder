<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_alat');
	}

	public function get_all()
	{
		$alats = $this->M_alat->get_all();

		if ( !$alats )
		{
			$result = array(
				'message' => 'No data found.'
			);

			$this->response(404, $result);
		}

		$result = array(
			'alat' => $alats
		);

		$this->response(200, $result);
	}

	public function update()
	{
		$data = $this->input->get();

		if ($this->M_alat->edit($data))
		{
			$result = array(
				'message' => 'Alat berhasil diubah.'
			);

			$this->response(200, $result);
		}
		else
		{
			$result = array(
				'message' => 'Terjadi error, gagal mengubah data alat.',
				'error'   => 'Update DB error'
			);

			$this->response(500, $result);
		}
	}

	public function get()
	{
		$id = $this->input->get('id');

		$alat = $this->M_alat->get($id);

		if ( !$alat )
		{
			$result = array(
				'message' => 'No data found.'
			);

			$this->response(404, $result);
		}

		$result = array(
			'alat' => $alat
		);

		$this->response(200, $result);
	}

}

/* End of file Alat.php */
/* Location: ./application/controllers/api/Alat.php */