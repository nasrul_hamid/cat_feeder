<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		$content['page_view'] = 'dashboard';
		$content['title']     = 'Dashboard';

		$this->load->view('template', $content);
	}

	public function food_stock()
	{
		$this->load->model('M_makanan');

		$food = $this->M_makanan->get_active_food();

		if ( !$food )
		{
			$result = array(
				'status'  => array('code' => 404, 'descripton' => 'No active food'),
				'message' => 'No active food'
			);

			$this->response(200, $result);
		}

		$result = array(
			'status' => array('code' => 200, 'descripton' => 'Ok'),
			'food'   => $food
		);

		$this->response(200, $result);
	}

	public function status_alat()
	{
		$this->load->model('M_alat');

		$alat = $this->M_alat->status(1);

		$result = array(
        	'alat' => $alat
		);

		$this->response(200, $result);
	}

	public function konsumsi_makanan()
	{
		$get = $this->input->get();

		$this->load->model('M_log');

		$konsumsi = $this->M_log->konsumsi_makanan(@$get['start_date'], @$get['end_date']);

		if ( !$konsumsi )
		{
			$result = array(
				'status'  => array('code' => 404, 'descripton' => 'No data found'),
				'message' => 'No data found'
			);

			$this->response(200, $result);
		}

		$result = array(
			'status'   => array('code' => 200, 'descripton' => 'OK'),
			'konsumsi' => $konsumsi
		);

		$this->response(200, $result);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */