<section class="content-header">
  <h1>
    Dashboard
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-6">
      <div class="box box-primary" id="box-chart">
        <div class="box-header">
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" id="refresh"><i class="fa fa-refresh"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="form-inline">
            <div class="form-group">
              <input type="text" name="start_date" class="form-control datepicker" placeholder="Start date">
            </div>
            <div class="form-group">
              <input type="text" name="end_date" class="form-control datepicker" placeholder="End date">
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-info"><i class="fa fa-search"></i></button>
            </div>
          </div>
          <div class="chart">
            <div id="container" style="width:100%; height:300px;"></div>
            <p class="text-center" id="chart-msg" style="display: none;"></p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="ion-pizza"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Stok Makanan</span>
          <span class="info-box-number"></span>

          <div class="progress">
            <div class="progress-bar" style="width: 40%"></div>
          </div>
          <span class="progress-description">
            40% Increase in 30 Days
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <div class="box box-primary" id="box-status-alat">
        <div class="box-header">
          <h3 class="box-title">Status Alat</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" id="refresh"><i class="fa fa-refresh"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-5">
              <img
                id="lamp-img" 
                src="<?=base_url('assets/img/pic_bulbon.gif')?>" 
                alt="">
            </div>
            <div class="col-md-7">
              <table id="alat-desc">
                <tr>
                  <td><span class="fa fa-credit-card"></span> Nama</td>
                  <td>:</td>
                  <td data-name="nama"></td>
                </tr>
                <tr>
                  <td><span class="fa fa-clock-o"></span> Update Interval</td>
                  <td>:</td>
                  <td data-name="update_interval"></td>
                </tr>
                <tr>
                  <td><span class="fa fa-desktop"></span> Status</td>
                  <td>:</td>
                  <td data-name="status"></td>
                </tr>
                <tr>
                  <td><span class="fa fa-cloud-upload"></span> Last Update</td>
                  <td>:</td>
                  <td data-name="last_update"></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
</section>

<!-- Highchart -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- Page js -->
<script src="<?=base_url('assets/js')?>/dashboard.js" type="text/javascript"></script>