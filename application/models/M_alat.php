<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_alat extends CI_Model {

	public function __construct()	
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function get_all()
	{
		$alats = $this->db->get('alat');

		return ($alats->num_rows() > 0) ? $alats->result_array() : array();
	}

	public function add($data = array())
	{
		$data['updated_at'] = date('Y-m-d H:i:s');
		return $this->db->insert('alat', $data);
	}

	public function get($id = NULL)
	{
		return $this->db->get_where('alat', array('id' => $id))
		                ->row_array();
	}

	public function edit($data = NULL)
	{
		$id = $data['id']; unset($data['id']);

		$data['updated_at'] = date('Y-m-d H:i:s');
		return $this->db->update('alat', $data, array('id' => $id));
	}

	public function delete($id = NULL)
	{
		return $this->db->delete('alat', array('id' => $id));
	}

	public function status($id = NULL)
	{
		$this->load->helper('custom_helper');
		
		// Ambil konidisi alat saat ini
		$alat = $this->get($id);

		// Cek last update alat, apakah masih diupdate
		// dalam interval yang tentukan
		$updated_at = strtotime($alat['updated_at']);
		$now        = strtotime(date('Y-m-d H:i:s'));
		$diff_sec   = $now - $updated_at;
		
		// Jika sudah tidak diupdate maka ubah status alat
		// menjadi OFF
		if ($diff_sec > $alat['update_interval'])
		{
			$this->db->update('alat', array('status'=>'OFF'), array('id'=>$id));
		}

		// return data alat terbaru
		$alat = $this->get($id);

		// tambahkan data differencenya dan 
		// current date nya
		$alat['now']  = date('Y-m-d H:i:s');
		$alat['diff'] = $diff_sec;
		$alat['updated_at'] = date('d M Y - H:i:s', strtotime($alat['updated_at']));
		

		return $alat;
	}

}

/* End of file M_alat.php */
/* Location: ./application/models/M_alat.php */