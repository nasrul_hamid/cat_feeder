<section class="content-header">
  <h1>
    Add Jadwal
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Add Jadwal</li>
  </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<form 
					action="<?=site_url('setting_jadwal/do_add')?>"
					method="POST">
					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jam</label>
								<div class="input-group clockpicker">
	                <input type="text" class="form-control" name="jam" required>
	                <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-time"></span>
	                </span>
	              </div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Jumlah Makanan (gram)</label>
								<input type="number" class="form-control" name="jumlah_makan" required>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="pull-right">
							<a href="javascript: window.history.back();" class="btn btn-danger">Back</a>
							<button type="submit" class="btn btn-success">Submit</button>	
						</div>
						
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script src="<?=base_url('assets/js')?>/setting_jadwal_add.js" type="text/javascript"></script>