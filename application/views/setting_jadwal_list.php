<section class="content-header">
  <h1>
    Setting Jadwal
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Setting Jadwal</li>
  </ol>
</section>

<section class="content">
  <?=ch_falert()?>
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <a href="<?=site_url('setting_jadwal/add')?>" class="btn btn-primary">Add</a>
        </div>
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover" id="sch-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Jam</th>
                <th>Jumlah Makanan (gram)</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($schedules as $schedule): ?>
              <tr>
                <td><?=$no++?></td>
                <td data-name="jam"><?=date('H:i', strtotime($schedule['jam']))?></td>
                <td><?=$schedule['jumlah_makan']?></td>
                <td>
                  <a 
                    href="<?=site_url('setting_jadwal/edit/'.$schedule['id'])?>" 
                    title="Edit"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;
                  <a 
                    href="#" 
                    title="delete" 
                    id="delete"
                    data-id="<?=$schedule['id']?>"
                    style="color: red"><span class="fa fa-trash"></span></a>
                </td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="<?=base_url('assets/js')?>/setting_jadwal.js" type="text/javascript"></script>