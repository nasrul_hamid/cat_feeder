<section class="content-header">
  <h1>
    Pengaturan Alat
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Pengaturan Alat</li>
  </ol>
</section>

<section class="content">
  <?=ch_falert()?>
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <a href="<?=site_url('alat/add')?>" class="btn btn-primary">Add</a>
        </div>
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover" id="alat-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Update Interval (detik)</th>
                <th>Status</th>
                <th>Updated At</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($alats as $alat): ?>
              <tr>
                <td><?=$no++?></td>
                <td data-name="nama"><?=$alat['nama']?></td>
                <td><?=$alat['update_interval']?></td>
                <td><?=($alat['status']=='ON')?'<span class="label label-success">ON</span>':'<span class="label label-danger">OFF</span>'?></td>
                <td><?=date('d M Y - H:i:s', strtotime($alat['updated_at']))?></td>
                <td>
                  <a 
                    href="<?=site_url('alat/edit/'.$alat['id'])?>" 
                    title="Edit"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;
                  <a 
                    href="#" 
                    title="delete" 
                    id="delete"
                    data-id="<?=$alat['id']?>"
                    style="color: red"><span class="fa fa-trash"></span></a>
                </td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="<?=base_url('assets/js')?>/alat_list.js"></script>