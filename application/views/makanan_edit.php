<section class="content-header">
  <h1>
    Tambah Makanan
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Tambah Makanan</li>
  </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<form 
					action="<?=site_url('makanan/do_edit')?>"
					method="POST">

					<!-- hidden -->
					<input type="hidden" name="id" value="<?=$makanan['id']?>">

					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="nama" class="form-control" value="<?=$makanan['nama']?>">
							</div>
							<div class="form-group">
								<label>Stock</label>
								<input type="number" name="initial_stock" class="form-control" value="<?=$makanan['initial_stock']?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Status</label>
								<select 
									name="status" 
									class="form-control">
									<option value=""></option>
									<option value="Active" <?=($makanan['status']=='Active')?'selected':''?>>Active</option>
									<option value="Inactive" <?=($makanan['status']=='Inactive')?'selected':''?>>Inactive</option>
								</select>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="pull-right">
							<a href="javascript: window.history.back();" class="btn btn-danger">Back</a>
							<button type="submit" class="btn btn-success">Submit</button>	
						</div>
						
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script src="<?=base_url('assets/js')?>/setting_jadwal_add.js" type="text/javascript"></script>