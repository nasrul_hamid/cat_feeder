<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_jadwal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_jadwal');

		$this->load->helper('custom_helper');
	}

	public function index()
	{
		$content['schedules'] = $this->M_jadwal->get_all();
		$content['page_view'] = 'setting_jadwal_list';
		$content['title']     = 'Setting Jadwal';

		$this->load->view('template', $content);
	}

	public function delete()
	{
		$id = $this->input->post('id');

		if ($this->M_jadwal->delete($id))
		{
			ch_set_flash('success', 'Jadwal berhasil didelete.');
		}
		else
		{
			ch_set_flash('failed', 'Terjadi error, jadwal gagal didelete.');
		}
	}

	public function add()
	{
		$content['page_view'] = 'setting_jadwal_add';
		$content['title']     = 'Add Jadwal';

		$this->load->view('template', $content);
	}

	public function do_add()
	{
		$post = $this->input->post();

		if ($this->M_jadwal->add($post))
		{
			ch_set_flash('success', 'Jadwal berhasil disimpan.');
		}
		else
		{
			ch_set_flash('failed', 'Terjadi error, jadwal gagal disimpan.');
		}

		redirect('setting_jadwal');
	}

	public function edit($id = NULL)
	{
		if ($id == NULL)
		{
			show_404();
			exit();
		}

		$content['schedule']  = $this->M_jadwal->get($id);
		$content['page_view'] = 'setting_jadwal_edit';
		$content['title']     = 'Add Jadwal';

		$this->load->view('template', $content);
	}

	public function do_edit()
	{
		$post = $this->input->post();

		if ($this->M_jadwal->edit($post))
		{
			ch_set_flash('success', 'Jadwal berhasil diubah.');
		}
		else
		{
			ch_set_flash('failed', 'Terjadi error, jadwal gagal diubah.');
		}

		redirect('setting_jadwal');
	}

}

/* End of file Setting_jadwal.php */
/* Location: ./application/controllers/Setting_jadwal.php */