//-------------------------------------------------------------------------------------
// CAT-FEEDER
// Sistem tertanam dan Robotika
// Kelompok-3 AJ-11
//-------------------------------------------------------------------------------------

#include <Q2HX711.h>
#include <Servo.h>

#define KNOWN_WEIGHT 90.0f    // Bobot referensi input kalibrasi
#define OFFSET 8498792      // nilai OFFSET HX711 dari output kalibrasi
#define RATIO 40208.0f      // nilai rasio HX711 dari output kalibrasi
#define DISP_AMT 50         // takaran penyajian sekali makan
#define DISP_N_MAX 3        // jumlah iterasi dispense maksimum

#define PIN_HX711_SCK A2    // pin SCK HX711
#define PIN_HX711_DT A3     // pin DT HX711
#define PIN_SERVO 5         // PIN SERVO

//HX711 constructor (dout pin, sck pin)
Q2HX711 LoadCell(PIN_HX711_DT, PIN_HX711_SCK);
Servo myservo;

long t;
bool is_feed_time;
int pos = 0;

float val = 0.00f, count = 0.00f;

float getGram(){
  //count++;  val = ((count-1)/count) * val    +  (1/count) * LoadCell.read();
  //val = 0.5 * val    +   0.5 * LoadCell.read();  
  val = LoadCell.read();
  return ( (val - OFFSET) / RATIO * KNOWN_WEIGHT );
}

void setup() {
  Serial.begin(9600);
  Serial.println("INISIALISASI ...");

  // SERVO SETUP
  myservo.attach(PIN_SERVO);

  // LOAD CELL SETUP
  
  Serial.println("SISTEM SIAP ...");
}

void loop() { 
  
  if(is_feed_time){
    Serial.println("FEEDING TIME :) ");
    
    // get current weight
    float w, w0 = getGram();

    int n = 0;
    while(((w - w0) < DISP_AMT) && (n < DISP_N_MAX)){
      Serial.print("Sebelum: ");Serial.print(w0);Serial.print(" gram.");
      
      // FEEDING
      myservo.write(180);
      delay(500);
      myservo.write(0);  
      delay(1000);

      // update current weight        
      w = getGram();    
      Serial.print("\tSesudah: ");Serial.print(w);Serial.print(" grams. ");
      Serial.print("\tDispensed: ");Serial.print(w - w0);Serial.println(" grams. ");
      n++;
    }
    
    is_feed_time = false;

    Serial.println("Meals ready :)");
  }

  // TERMINAL CONSOLE
  if (Serial.available() > 0) {
    char inByte = Serial.read();
    if (inByte == 'f') is_feed_time = true;
  }

}
